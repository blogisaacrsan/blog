+++
title = "About this site"
description = "isaacrsan's blog"
date = 2023-01-19T22:35:00-06:00
showLicense = false
showToc = false
+++

This blog contains articles about software, cybersecurity, computers, code , programs, FOSS and other interesting things.

If you want to know more information about a post or have comments, you can send me a [email](mailto:isaac@rsan.org)
